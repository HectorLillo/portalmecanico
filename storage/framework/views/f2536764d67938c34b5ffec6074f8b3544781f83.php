<html lang="es">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">

        <title>Powergot</title>

    </head>
    <body>
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                	<?php echo $__env->yieldContent('content'); ?>
                </div>
            </div>
        </div>
    </body>
</html>
<?php /**PATH /var/www/portal/resources/views/layout.blade.php ENDPATH**/ ?>