<?php $__env->startSection('content'); ?>

    <div class="row">
        <div id="app" class="col-lg-12 pl-5 pr-5">
            <quotations-component></quotations-component>
        </div>
    </div>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.portalapp', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /var/www/portal/resources/views/admin/cotizaciones.blade.php ENDPATH**/ ?>