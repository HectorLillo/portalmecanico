<?php $__env->startSection('content'); ?>

    <div class="row">
        <div id="app" class="col-lg-12 pl-5 pr-5">
            <code-component></code-component>
        </div>
    </div>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.portalapp', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\portalmecanico\resources\views/admin/codigos.blade.php ENDPATH**/ ?>