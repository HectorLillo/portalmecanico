require('bootstrap')
window.Vue = require('vue')

// Vue.config.devtools = false
// Vue.config.debug = false
// Vue.config.silent = true


import { ValidationProvider } from 'vee-validate'
import Vue2Filters from 'vue2-filters'

// Register it globally
// main.js or any entry file.

import vSelect from 'vue-select'

Vue.component('v-select', vSelect)
Vue.component('ValidationProvider', ValidationProvider)
Vue.use(Vue2Filters)

import store from './components/store/index'

//////////////////////////////////////////////////////////////////////////////////////////////////

Vue.component('boleta-component', require('./components/Boleta/Index.vue'))

//////////////////////////////////////////////////////////////////////////////////////////////////

Vue.component('vehicle-mechanic-component', require('./components/VehicleMechanic/Index.vue'))
Vue.component('vehicle-component', require('./components/Vehicle/Index.vue'))
Vue.component('vehicle-brand-component', require('./components/VehicleBrand/Index.vue'))
Vue.component('vehicle-model-component', require('./components/VehicleModel/Index.vue'))

//seccion de roles y permisos para permisos
Vue.component('user-roles-component', require('./components/Roles/Users.vue'))
Vue.component('componente-usuario', require('./components/User/Index.vue'))
Vue.component('componente-mecanico-usuario', require('./components/UserMechanic/Index.vue'))
Vue.component('roles-component', require('./components/Roles/Roles.vue'))

Vue.component('notes-component', require('./components/Note/Index.vue'))
Vue.component('quotations-component', require('./components/Quotation/Index.vue'))
Vue.component('quotationsclient-component', require('./components/Quotationclient/Index.vue'))
Vue.component('quotationuser-component', require('./components/Quotationuser/Index.vue'))

Vue.component('imports-component', require('./components/Import/Index.vue'))

Vue.component('clients-component', require('./components/Client/Index.vue'))
Vue.component('products-component', require('./components/Product/Index.vue'))
Vue.component('code-component', require('./components/Inventory/Code.vue'))
Vue.component('inventario-component', require('./components/Inventario/Index.vue'))

Vue.component('detalle-usuario-component', require('./components/User/Detalle.vue'))
Vue.component('sales-component', require('./components/Sales/Index.vue'))

new Vue({
    el: '#app',
    store: store,
    ready: function()  {
        this.$nextTick(function () {
            $('[data-toggle="tooltip"]').tooltip()
        });
    },
});


