<?php

header("Content-Type: application/json");
header('Access-Control-Allow-Origin: *');
header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept");
header('Access-Control-Allow-Methods: GET, POST, PUT, DELETE');

$numerosSumar = json_decode(file_get_contents('php://input'), true);

$resul=$numerosSumar['resultado'];

$lineaCompleta = $numerosSumar['lineaCompleta'];
$sumaTotalBoleta=(int)$numerosSumar['sumaTotalBoleta'];

$fecha = $numerosSumar['fecha'];
$giro = $numerosSumar['giroEmisor'];
$dir = $numerosSumar['dirOrigen'];
$rut_r = $numerosSumar['rutReceptor'];
$numPrecio=(int)$numerosSumar['precio'];
$productoEx=$numerosSumar['producto'];
$numCantidad=(int)$numerosSumar['cantidad'];

$total=$numPrecio*$numCantidad;

if ($fecha == NULL || $giro == NULL || $dir == NULL || $rut_r == NULL) 
{
	$resul="Falta rellenar datos";
	echo $resul;
}
else
{
	$curl = curl_init();
	curl_setopt_array($curl, array(
	CURLOPT_URL => "https://dev-api.haulmer.com/v2/dte/document",
	CURLOPT_RETURNTRANSFER => true,
	CURLOPT_ENCODING => "",
	CURLOPT_MAXREDIRS => 10,
	CURLOPT_TIMEOUT => 0,
	CURLOPT_FOLLOWLOCATION => true,
	CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
	CURLOPT_CUSTOMREQUEST => "POST",
	CURLOPT_POSTFIELDS =>"{\"response\":[\"PDF\"],\"dte\":{\"Encabezado\":{\"IdDoc\":{\"TipoDTE\":39,\"Folio\":0,\"FchEmis\":\"{$fecha}\",\"IndServicio\":\"3\"},\"Emisor\":{\"RUTEmisor\":\"76795561-8\",\"RznSocEmisor\":\"HAULMER SPA\",\"GiroEmisor\":\"{$giro};COMERCIOELEC\",\"CdgSIISucur\":\"81303347\",\"DirOrigen\":\"{$dir}\",\"CmnaOrigen\":\"Curicó\"},\"Receptor\":{\"RUTRecep\":\"{$rut_r}\"},\"Totales\":{\"MntTotal\":{$sumaTotalBoleta},\"TotalPeriodo\":{$sumaTotalBoleta},\"VlrPagar\":{$sumaTotalBoleta}}},\"Detalle\":[{$lineaCompleta}]}}",
	CURLOPT_HTTPHEADER => array(
	"apikey: 928e15a2d14d4a6292345f04960f4bd3",
	"Content-Type: application/json"
	),
	));

	$response = curl_exec($curl);
	curl_close($curl);

////////////////////////////////////////////////////////Guardado.

	$resul=$response;
	date_default_timezone_set("America/Santiago");
	$hora = date("G.i.s");
	$mes = date("Y-m-d");
	$fecha="F({$mes})&H({$hora})";
	
	$pdf_decoded = base64_decode ($resul);
	
	$pdf = fopen ("{$fecha}.pdf","w");
	fwrite ($pdf,$pdf_decoded);
	fclose ($pdf);

//////////////////////////////////////////////////////////Guardar en la Base de datos.
	
	$servidor="localhost";
	$usuario="root";
	$clave="";
	$baseDeDatos="basenuevo";
	$enlace = mysqli_connect($servidor, $usuario, $clave, $baseDeDatos);
	$sql = "INSERT INTO boleta (fecha, ruta) VALUES ('{$fecha}.pdf', '/Boleta/BoletasEmitidas_PHP/{$fecha}.pdf')";
	if (mysqli_query($enlace, $sql)){} 
	mysqli_close($enlace);
	
//////////////////////////////////////////////////////////Finaliza. 
 
	$resul="{$fecha}.pdf";
	echo $resul;
}
?>

